<?php

require_once("model/Manager.php");
class ContactManager extends Manager {

  public function getMessages() {
    $db = $this->dbConnect();
    $req = $db->query('SELECT `id`, `name`, `content`, `subject`, `email` FROM messages');
    $messages = $req->fetchAll();
    return $messages;
  }
  
  public function getMessage($messageId) {
    $db = $this->dbConnect();
    $req = $db->prepare('SELECT `id`, `name`, `content`,`subject`, `email`, DATE_FORMAT(`created_date`, "%d/%m/%Y à %Hh%i") AS comment_date FROM messages WHERE id = ?');
    $req->execute(array($messageId));
    $message = $req->fetch();
    return $message;
  }

  public function deleteMessage($idMessage) {
    $db = $this->dbConnect();
    $delete = $db->prepare('DELETE FROM messages WHERE id = ?');
    $delete->execute(array($idMessage));
    header('Location: messagerie');
  }
  
  public function addMessage($name, $content, $subject, $email) {
    $db = $this->dbConnect();
    $messages = $db->prepare('INSERT INTO messages (`name`, `content`,`subject`, `email`, `created_date`) VALUES(?, ?, ?, ?, NOW())');
    $affectedLines = $messages->execute(array($name, $content, $subject, $email));
    return $affectedLines;
  }
  
}