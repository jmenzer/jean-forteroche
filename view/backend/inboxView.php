<?php $title = " Jean Forteroche - Boite de réception" ?>
<?php $header = "header-backend" ?>
<?php $headerTop = "header-top" ?>
<?php ob_start(); ?>


<section class="template-middle">
  <h1 class="title-page">Boite de réception</h1>
  <h2 class="subtitle-page">Retrouvez tous les messages de vos utilisateurs, envoyés par le formulaire de contact.</h2>
  <a href="administration">
    <div class="goback">
      <i class="far fa-arrow-alt-circle-left"></i>
      <span class="goback-text">Retour</span>
    </div>
  </a>

  <?php 
    if (!empty($messages)) {
  ?>
    <ul>  
      <?php
        for ($i = 0; $i < count($messages); $i++)
        {
      ?>
        <li class="feature">
          <a href="index.php?action=voir-message&id=<?= $messages[$i]["id"] ?>">
            <?= htmlspecialchars($messages[$i]['subject']); ?>
          </a>
        </li>
      <?php
        }
      ?>
    </ul>
  <?php
    } else {
  ?>
    <h3 class="subtitle-page">
      Vous n'avez aucun message.
    </h3>
  <?php
    }
  ?>
</section>


<?php $content = ob_get_clean(); ?>
<?php require('view/backend/template.php'); ?>