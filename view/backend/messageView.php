<?php $title = " Messagerie - Jean Forteroche" ?>
<?php $header = "header-backend" ?>
<?php $headerTop = "header-top" ?>
<?php ob_start(); ?>


<section class="template-middle">
  <div class="template-middle">
    <h1 class="title-page">Message</h1>
    
    <a href="messagerie">
      <div class="goback">
        <i class="far fa-arrow-alt-circle-left"></i>
        <span class="goback-text">Retour</span>
      </div>
    </a>

    <div class="backend-form">
      <div class="message-inbox-top">
        <div class="form-field-title">
          Sujet
          <div class="input-text"> <?= $message['subject'] ?></div>
        </div>
        <div class="form-field-title">
          Nom
          <div class="input-text"> <?= $message['name'] ?></div>
        </div>
        <div class="form-field-title">
          Mail
          <div class="input-text"> <?= $message['email'] ?></div>
        </div>
      </div>
      <div class="message-inbox-middle">
        Reçu le
        <div class="input-text"> <?= $message['comment_date'] ?></div>
      </div>
      <div class="message-inbox-bottom">
        Message
        <div class="input-text"> <?= $message['content'] ?></div>
      </div>
      <div class="two-choose">
        <a class="submit-btn" href="mailto:<?= $message['email'] ?>">Répondre</a>
        <button class="submit-btn" onclick="Delete()" >Supprimer</button>
      </div>
    </div>
  </div>
</section>


<script>
  function Delete() {
    if (confirm("Etes vous sur de vouloir supprimer ce message ?"))
    {
      window.location.href = "index.php?action=supprimer-message&id=<?= $message['id'] ?>";
    } else {
      console.log("annuler");
    }
  }
</script>


<?php $content = ob_get_clean(); ?>
<?php require('view/backend/template.php'); ?>

