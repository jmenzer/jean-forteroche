<?php $title = " Un billet Pour l'Alaska - Le roman de Jean Forteroche" ?>
<?php $header = "header" ?>
<?php $headerTop = "header-top" ?>
<?php ob_start(); ?>


<section class="template-big">
  <div class="template-home">
    <ul class="quote-slider">
      <li id='quote0' style='right: 20%'>
        "Un véritable chef d'œuvre des temps modernes."
        <div class="site">Le Figaro</div>
      </li>
      <li id='quote1'>
        "Jean revient avec un style certain et la maturité d'un auteur de renom."
        <div class="site">Le Point</div>
      </li>
      <li id='quote2'>
        "Ce thriller est aussi haletant, tant par son suspens que par la description de l'environnement."
        <div class="site">France Inter</div>
      </li>
      <li id='quote3'>
        "20 ans après 'Meurtre à Oslo', Jean Forteroche signe son un roman engagé et bouleversant."
        <div class="site">Le Monde</div>
      </li>
      <li id='quote4'>
        "Certainement, le nouveau best-seller de la rentrée littéraire."
        <div class="site">Libération</div>
      </li>
    </ul> 

    <div class="content-general">
      <div class="content-home-left">
        <h2 class="subtitle-page">L'histoire</h2>
        <p>Miles Halter a seize ans, mais n'a pas l'impression d'avoir vécu. Assoiffé d'expériences, 
          il quitte le cocon familial pour le campus universitaire : ce sera le lieu de tous les possibles, 
          de toutes les premières fois. Et de sa rencontre avec l'Alaska. La troublante, l'insaisissable, 
          insoumise et fascinante. Amitiés fortes, amour, transgression, quête de sens.
        </p>
      </div>
      <div class="content-home-right">
        <div class="jean"></div>
        <div class="jean-abstract">
          Jean Forteroche est né en 1976 à Amiens. Très tôt attiré par la littérature classique,
          il publie son premier roman, "Soleil de plomb" à l'âge de 17 ans.
        </div>
      </div>
    </div>
  </div>
  
  <div class="content-redirect">
    <h2><a href="chapitres">Voir les chapitres</a></h2>
  </div>
</section>


<?php $content = ob_get_clean(); ?>
<?php require('view/frontend/template.php'); ?>