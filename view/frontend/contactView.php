<?php $title = " Un billet Pour l'Alaska - Formulaire de contact" ?>
<?php $header = "header" ?>
<?php $headerTop = "header-top" ?>
<?php ob_start(); ?>


<section class="template-short">
   <h1 class="title-page">Formulaire de contact</h1>
   <form class="contact-form" action="ajout-message" method="post">
     <div class="contact-form-top">
      <div class="form-field">
         <input id="name" name="name" class="input-text" type="text" placeholder="Entrez votre nom" required>
      </div>
      <div class="form-field">
         <input id="subject" name="subject" class="input-text" type="text" placeholder="Le sujet de votre message" required>
      </div>
      <div class="form-field">
         <input id="email" name="email" class="input-text" type="email" placeholder="Renseignez votre mail" required>
      </div>
     </div>
     <div class="contact-form-content">
      <div class="form-field">
         <textarea id="content" name="content" class="input-text" type="text" placeholder="Entrez votre message, vous recevrez une réponse sous 48h." required></textarea>
      </div>
      <div class="form-field">
         <input class="submit-btn" type="submit" value="Envoyer">
      </div>
    </div>  
   </form>
</section>


<?php $content = ob_get_clean(); ?>
<?php require('view/frontend/template.php'); ?>